#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <fstream>
#include <sstream>
using namespace std;

#define FNAME_TAG "-f "
#define LNAME_TAG "-l "
#define PHONE_TAG "-p "
#define EMAIL_TAG "-e "
#define SUCCESS "Command Ok"
#define FAIL "Command Failed"
#define NULL_STR ""
//Functions:
#define ADD "add"
#define SEARCH "search"
#define DELETE "delete"
#define UPDATE "update"
// add / search / delete / update

class contact{
    public:

    int id;
    string fname;
    string lname;
    string email;
    string phone;
};

/* Main Functions */
void set_contacts_from_file(vector<contact>& mycontact, int& id_counter); // fill mycontact from csv file
void decode_input(string input, contact* user_contact, string* function_name); // fill user_contact whith users input
void process_input(string& input, vector<contact>& mycontact, contact& user_contact, string& function_name, int& id_counter); // process input function
void print_output(contact user_contact, string function_name); // print SUCCESS or FAIL
void save_contacts_to_file(vector<contact> mycontact); // create contacts.csv and fill with mycontact
/* Main Functions */

/* Sub functions */
string decode_tag(string* input, string tag); // return what is after tag & before next tag or eol
bool add_contact(vector<contact>& mycontact, contact& user_contact, int& id_counter);
bool delete_contact(vector<contact>& mycontact, string id);
bool update_contact(vector<contact>& mycontact, contact& user_contact);
void find_print_contact(string keyword, vector<contact> mycontact); // find and print all contacts

void csv_decode(string line, contact& new_contact); // line -> new_contact
void csv_encode(string& line, contact user_contact); // user_contact -> line
/* Sub functions */

/* Little functions */
string valid_email(string email); // valid->email // illegal->NULL_STR
string valid_phone(string phone); // valid->phone // illegal->NULL_STR
int strpos(string s, string search_word, int start_pos = 0); //Return search_word place in s string // -1 Not Found
bool strdel(string* s, string delete_word); //Delete all delete_words in s string
bool isdigit(string a); // string is all digits?!
void print_contact(contact user_contact); // print single contact info
/* Little functions */

int main()
{
    string input;
    vector <contact> mycontact;
    contact user_contact; // user new contact info
    string function_name; // add - search - delete - update - NULL_STR:'input was wrong!'
    int id_counter=0; // id generator!

    set_contacts_from_file(mycontact, id_counter); // read csv file
    while(getline (cin, input)){
	    decode_input(input, &user_contact, &function_name);
	    //cerr << "//////" << user_contact.id << endl << user_contact.fname << endl << user_contact.lname;
	    //cerr << endl << user_contact.phone << endl << user_contact.email << endl << function_name << "///////" << endl;
	    process_input(input, mycontact, user_contact, function_name, id_counter);
	    print_output(user_contact, function_name);
		save_contacts_to_file(mycontact);
	}

    return 0;
}




/* Main Functions */
void decode_input(string input, contact* user_contact, string* function_name){
	if(strpos(input,ADD)!=-1){
		//add -l Hajiloo -e m.taghi@gmail.com -f Mohammad -p 09191991919
		user_contact->fname = decode_tag(&input, FNAME_TAG);
		user_contact->lname = decode_tag(&input, LNAME_TAG);
		user_contact->phone = valid_phone(decode_tag(&input, PHONE_TAG));
		user_contact->email = valid_email(decode_tag(&input, EMAIL_TAG));
		if(user_contact->fname == NULL_STR || user_contact->lname == NULL_STR || user_contact->phone == NULL_STR || user_contact->email == NULL_STR){
			*function_name = NULL_STR;
		}else{
			*function_name = ADD;
		}

	}else if(strpos(input, SEARCH)!=-1){
		//search keyword
		string keyword = decode_tag(&input, " ");
		user_contact->fname=keyword; // save search keyword in fname
		*function_name = SEARCH;

	}else if(strpos(input, DELETE)!=-1){
		//delete id
		string id = decode_tag(&input, DELETE);
		user_contact->fname=id; // save delete id in fname
		*function_name = DELETE;

	}else if(strpos(input,UPDATE)!=-1){
		//update <id> -l Hajiloo -e m.taghi@gmail.com -f Mohammad -p 09191991919
		string id_string = decode_tag(&input, UPDATE); // decode id
		if(id_string!=NULL_STR){
			user_contact->id = atoi(&(id_string[0]));
		}else{
			user_contact->id = -1;
		}
		user_contact->fname = decode_tag(&input, FNAME_TAG);
		user_contact->lname = decode_tag(&input, LNAME_TAG);
		user_contact->phone = valid_phone(decode_tag(&input, PHONE_TAG));
		user_contact->email = valid_email(decode_tag(&input, EMAIL_TAG));
		if(user_contact->id == -1 || (user_contact->fname == NULL_STR && user_contact->lname == NULL_STR && user_contact->phone == NULL_STR && user_contact->email == NULL_STR)){
			*function_name = NULL_STR;
		}else{
			*function_name = UPDATE;
		}
	}else{
		*function_name = NULL_STR;
	}
}

void process_input(string& input, vector<contact>& mycontact, contact& user_contact, string& function_name, int& id_counter){
	if(function_name != NULL_STR){

		if(function_name == ADD){
			if(add_contact(mycontact, user_contact, id_counter) == false){
				function_name = NULL_STR;
			}

		}else if(function_name == SEARCH){
			string keyword = user_contact.fname;
			find_print_contact(keyword, mycontact);

		}else if(function_name == DELETE){
			string id = user_contact.fname;
			if(delete_contact(mycontact, id) == false){
				function_name = NULL_STR;
			}

		}else if(function_name == UPDATE){
			if(update_contact(mycontact, user_contact) == false){
				function_name = NULL_STR;
			}
		}
	}
}

void print_output(contact user_contact, string function_name){
	//cerr << function_name;
	if(function_name != SEARCH){
		if(function_name != NULL_STR){
			cout << SUCCESS << endl;
		}else{
			cout << FAIL << endl;
		}
	}

}

void set_contacts_from_file(vector<contact>& mycontact, int& id_counter){
	string line;
	contact new_contact;
	ifstream myfile( "contacts.csv" );
	if (myfile){
		getline( myfile, line ); // read first line
    	while (getline( myfile, line )){

      		if(line != ""){ // Line shoudn't be empty!
	      		csv_decode(line, new_contact);
	      		if(id_counter<=new_contact.id){
	      			id_counter=new_contact.id+1; // set id_counter=the last id+1
	      		}
	      		mycontact.push_back(new_contact); // Add contact
      		}
      	}
    myfile.close();
    }
}
void save_contacts_to_file(vector<contact> mycontact){
	ofstream myfile( "contacts.csv" ); //this creates it.
	myfile << "id,fname,lname,email,phone" << endl;
	string line;
	for(int i=0;i<mycontact.size();i++){
		csv_encode(line, mycontact[i]);
		myfile << mycontact[i].id << line << endl;
	}
}

/* Main Functions */




/* Sub functions */

string decode_tag(string* input, string tag){
	if(strpos(*input, tag)!=-1){
		string tag_data;
		int start_pos=strpos(*input, tag)+tag.length();
		int i=start_pos;
		bool flag=true;
		while(flag==true){
			if((*input)[i]=='\0' || i==strpos((*input), FNAME_TAG) || i==strpos((*input), LNAME_TAG) || i==strpos((*input), PHONE_TAG) || i==strpos((*input), EMAIL_TAG)){
				flag=false;
			}else{
				i++;
			}
		}
		tag_data.assign((*input), start_pos/*start*/, i-start_pos/*length*/);

		(*input).erase(start_pos/*start*/, i-start_pos/*length*/); //delete tag data
		strdel(input, tag);// delete command_tag
		strdel(&tag_data, " ");//delete spaces
		return tag_data;

	}else{
		return NULL_STR;
	}
}

bool add_contact(vector<contact>& mycontact, contact& user_contact, int& id_counter){
	bool flag=true;
	for(int i=0;i<mycontact.size();i++){
		if(user_contact.email == mycontact[i].email || user_contact.phone == mycontact[i].phone || (user_contact.fname == mycontact[i].fname && user_contact.lname == mycontact[i].lname) ){
            flag=false; // CHECK CONTACT DUPLICATION
		}
	}
	if(flag == true){ // ADD
		user_contact.id = id_counter;
   		mycontact.push_back(user_contact);
   		id_counter++;
	}

	return flag;
}
bool delete_contact(vector<contact>& mycontact, string id){
	int id_int = atoi(&(id[0]));
	for(int i=0;i<mycontact.size();i++){
		if(mycontact[i].id == id_int){
			mycontact.erase(mycontact.begin() + id_int); // DELETE
			return true;
		}
	}
	return false;
}
bool update_contact(vector<contact>& mycontact, contact& user_contact){
	bool flag=true;
	int save_contact_i=-1;
	for(int i=0;i<mycontact.size();i++){
		if(user_contact.email == mycontact[i].email || user_contact.phone == mycontact[i].phone || (user_contact.fname == mycontact[i].fname && user_contact.lname == mycontact[i].lname) ){
            flag=false; // CHECK CONTACT DUPLICATION
		}
		if(mycontact[i].id == user_contact.id){
			save_contact_i = i; // save i for updating // i!=-1 => contact is existed!
		}
	}
	if(save_contact_i != -1){
		flag =false;
	}
	if(flag == true){ // ADD
		if(user_contact.email != NULL_STR){
			mycontact[save_contact_i].email = user_contact.email;
		}
		if(user_contact.fname != NULL_STR){
			mycontact[save_contact_i].fname = user_contact.fname;
		}
		if(user_contact.lname != NULL_STR){
			mycontact[save_contact_i].lname = user_contact.lname;
		}
		if(user_contact.phone != NULL_STR){
			mycontact[save_contact_i].phone = user_contact.phone;
		}
	}

	return flag;
}


void find_print_contact(string keyword, vector<contact> mycontact){
	for(int i=0;i<mycontact.size();i++){
		if(strpos(mycontact[i].fname, keyword)!=-1 || strpos(mycontact[i].lname, keyword)!=-1 || strpos(mycontact[i].email, keyword)!=-1 || strpos(mycontact[i].phone, keyword)!=-1 ){
            print_contact(mycontact[i]);
		}
	}
}
void print_contact(contact user_contact){
	if(user_contact.id != -1){
		cout << user_contact.id << " " << user_contact.fname << " " << user_contact.lname << " " << user_contact.email << " " << user_contact.phone << endl ;
	}
}

void csv_decode(string line, contact& new_contact){
	string id;
	int pos=0;
	int start_pos=0;
	int length=0;

	length=strpos(line, ",");
	id.assign(line, start_pos, length);
	new_contact.id=atoi(&(id[0]));

	start_pos=strpos(line, ",")+1;
	length=strpos(line, ",", start_pos) - start_pos;
	new_contact.fname.assign(line, start_pos, length);

	start_pos=strpos(line, ",", start_pos)+1; // set start pos after next ","
	length=strpos(line, ",", start_pos) - start_pos; // set length next "," to past ","
	new_contact.lname.assign(line, start_pos, length);

	start_pos=strpos(line, ",", start_pos)+1;
	length=strpos(line, ",", start_pos) - start_pos;
	new_contact.email.assign(line, start_pos, length);

	start_pos=strpos(line, ",", start_pos)+1;
	length=strpos(line, ",", start_pos) - start_pos;
	new_contact.phone.assign(line, start_pos, length);
}
void csv_encode(string& line, contact user_contact){
	line.assign("");
    //I coudnt convert int to string in c++ 98!
	line.append(",");
	line.append(user_contact.fname); line.append(",");
	line.append(user_contact.lname); line.append(",");
	line.append(user_contact.email); line.append(",");
	line.append(user_contact.phone); line.append(",");
}
/* Sub functions */



/* Little functions */

string valid_email(string email){
	int at_sign = strpos(email, "@");
	int dot_sign = strpos(email, ".", strpos(email, "@"));
	if(at_sign!=-1 && dot_sign!=-1 && at_sign<dot_sign){
		return email;
	}else{
		return NULL_STR;
	}
}
string valid_phone(string phone){
	if(isdigit(phone) && phone.length()==11 && phone[0]=='0' && phone[1]=='9'){
		return phone;
	}else{
		return NULL_STR;
	}
}


bool isdigit(string a){
	bool flag=true;
	for(int i=0;i<a.length();i++){
		if(a[i]<48 || a[i]>57){
			flag=false;
		}
	}
	return flag;
}

int strpos(string s, string search_word, int start_pos){ //Return search_word place in s string // -1 Not Found
    for(int i=start_pos;s[i]!='\0';i++){
        bool flag=true;
        for(int j=0; j<search_word.length(); j++){
            if(s[i+j]!='\0'){
                if(s[i+j]!=search_word[j]){
                    flag=false;
                }
            }else{
                flag=false;
            }
        }
        if(flag==true){
            return i;
        }
    }
    return -1;

}

bool strdel(string* s, string delete_word){
    if (strpos(*s, delete_word)==-1){
		return false;
	}

    while(strpos(*s, delete_word)!=-1){
    	(*s).erase(strpos(*s, delete_word)/*start*/, delete_word.length()/*length*/);
    }
    return true;

}
/* Little functions */
